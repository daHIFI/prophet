import os
from datetime import datetime
import pandas as pd
from fbprophet import Prophet
from fbprophet.plot import plot_plotly
import matplotlib.pyplot as plt
import plotly.offline as py
import pickle
import numpy as np
import seaborn as sns
#from alphaVantageAPI.alphavantage import AlphaVantage

from alpha_vantage.timeseries import TimeSeries
from alpha_vantage.techindicators import TechIndicators
from dotenv import load_dotenv

load_dotenv()

ALPHAVANTAGE_API_KEY = os.getenv('ALPHAVANTAGE_API_KEY')
ts = TimeSeries(key=ALPHAVANTAGE_API_KEY, output_format='pandas')
ti = TechIndicators(key=ALPHAVANTAGE_API_KEY, output_format='pandas')


def alpha_df_to_prophet_df(df):
    df.reset_index(level=0, inplace=True)
    df = df[['date','4. close']]
    df.rename(columns={'date': 'ds', '4. close': 'y'}, inplace=True)
    df['ds'] = pd.to_datetime(df['ds'])
    df['y'] = df['y'].astype(float)
    return df


# https://github.com/RomelTorres/alpha_vantage/blob/develop/alpha_vantage/timeseries.py
def get_time_series(time_series, symbol, **kwargs):
    function = getattr(ts, time_series)
    data, meta_data = function(symbol=symbol, **kwargs)
    print(meta_data)
    return data


# https://www.alphavantage.co/documentation/#technical-indicators
# https://github.com/RomelTorres/alpha_vantage/blob/develop/alpha_vantage/techindicators.py
def get_technical(indicator, symbol, **kwargs):
    function = getattr(ti, indicator)
    data, meta_data = function(symbol=symbol, **kwargs)
    print(meta_data)
    return data


def get_ticker(symbol, outputsize='full'):
    ticker, meta_data = ts.get_daily(symbol=symbol, outputsize=outputsize)
    print(meta_data)
    return ticker


def plot_ticker(symbol):
    result = get_symbol(symbol)

    # plot together
    plt.plot(result.index, result['4. close'], result.index, result.SMA, result.index, result.RSI)
    plt.show()

    # plot rsi as subplot
    plt.subplot(211, title='Price')
    plt.plot(result.index, result['4. close'], result.index, result.SMA)
    plt.subplot(212, title="RSI")
    plt.plot(result.index, result.RSI)
    plt.show()


def get_symbol(symbol):
    CACHE_DIR = './cache'
    # check if cache exists
    symbol = symbol.upper()
    today = datetime.now().strftime("%Y_%m_%d")

    file = CACHE_DIR + '/' + symbol + '_' + today + '.pickle'
    if os.path.isfile(file):
        # load pickle
        print("{} Found".format(file))
        result = pickle.load(open(file, "rb"))
    else:
        # get data, save to pickle
        print("{} not found".format(file))
        ticker = get_time_series('get_daily', symbol, outputsize='full')
        sma = get_technical('get_sma', symbol, time_period=60)
        rsi = get_technical('get_rsi', symbol, time_period=60)

        frames = [ticker, sma, rsi]
        result = pd.concat(frames, axis=1)
        pickle.dump(result, open(file, "wb"))
        print("{} saved".format(file))
    return result

    # not sure how he got the trailing line or y-axis
    #plt.hist(ticker['4. close'], bins='auto')

    # plt.plot(ticker['5. volume'])
    # plt.show()


def prophet(ticker, fcast_time=360):
    ticker = alpha_df_to_prophet_df(ticker)

    df_prophet = Prophet(changepoint_prior_scale=0.15, daily_seasonality=True)
    df_prophet.fit(ticker)
    fcast_time = fcast_time
    df_forecast = df_prophet.make_future_dataframe(periods=fcast_time, freq='D')
    df_forecast = df_prophet.predict(df_forecast)
    df_prophet.plot(df_forecast, xlabel='Date', ylabel='Ticker Price')
    plt.show()

    df_prophet.plot_components(df_forecast)
    plt.show()

    fig = plot_plotly(df_prophet, df_forecast)  # This returns a plotly Figure
    py.plot(fig)

    return df_prophet, df_forecast

def prophet_bokeh(df_prophet, df_forecast):
    p = figure(x_axis_type='datetime')
    p.varea(y1='yhat_lower', y2='yhat_upper', x='ds', color='#0072B2', source=df_forecast, fill_alpha=0.2)
    p.line(df_prophet.history['ds'].dt.to_pydatetime(), df_prophet.history['y'], legend="History", line_color="black")
    p.line(df_forecast.ds, df_forecast.yhat, legend="Forecast", line_color='#0072B2')
    show(p)
